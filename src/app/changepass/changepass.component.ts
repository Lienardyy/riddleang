import { LogService } from './../log.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'app-changepass',
  templateUrl: './changepass.component.html',
  styleUrls: ['./changepass.component.css'],
  providers: [UserService, LogService]
})
export class ChangepassComponent implements OnInit {
  diffPass: boolean;
  success: boolean;
  private id;
  constructor(public fb: FormBuilder, private userService: UserService, private logService: LogService) { }

  public ChangePassForm = this.fb.group({
    password: ["", Validators.required],
    confirmPassword: ["", Validators.required],
  });

  ngOnInit() {
  }

  changePass(event) {
    if (this.ChangePassForm.value.confirmPassword != this.ChangePassForm.value.password ) {
      this.diffPass = true;
      this.success = false;
    } else {
      this.diffPass = false;
      this.logService.getUser().subscribe((res) => {
        this.id = res.id
        this.userService.changePassword(this.id, this.ChangePassForm.value.password).subscribe((user) => {
          this.success = true;
          this.ChangePassForm.reset();
        });
      });
    }
  }
}
