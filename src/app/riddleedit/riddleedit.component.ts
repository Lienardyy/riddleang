import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RiddleService } from '../riddle.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Riddle } from '../riddle';

@Component({
  selector: 'app-riddleedit',
  templateUrl: './riddleedit.component.html',
  styleUrls: ['./riddleedit.component.css']
})
export class RiddleeditComponent implements OnInit {
  id: number;
  questionModel: string;
  answerModel: string;

  constructor(private router: Router, public fb: FormBuilder, private route: ActivatedRoute, private riddleService: RiddleService) {
    this.id = route.snapshot.params['id'];
  }

  public editRiddleForm = this.fb.group({
    question: ["", Validators.required],
    answer: ["", Validators.required],
  });

  ngOnInit() {
    this.riddleService.getRiddle(this.id).subscribe(res => {
      this.questionModel = res['riddle']['question']; 
      this.answerModel = res['riddle']['answer']; 
    })
  }

  edit() {
    this.riddleService.edit(this.id, this.editRiddleForm.value.question, this.editRiddleForm.value.answer).subscribe(res => {
      this.router.navigate(['admin/riddle']);
    })
  }

}
