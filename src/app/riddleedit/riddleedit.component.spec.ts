import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiddleeditComponent } from './riddleedit.component';

describe('RiddleeditComponent', () => {
  let component: RiddleeditComponent;
  let fixture: ComponentFixture<RiddleeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiddleeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiddleeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
