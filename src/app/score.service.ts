import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {Score} from './score';

@Injectable()
export class ScoreService {

  constructor(private http: Http) {}

    private usersUrl = "http://localhost:8000/api/scores";

    getScores(id: number): Observable<Score[]> {

        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",            
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.get(this.usersUrl + "/" + id, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }

    addScore(user_id: number, score: number): Observable<Score[]> {
        var data = JSON.stringify({user_id: user_id, score: score});
        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",            
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });
        
        return this.http.post(this.usersUrl, data, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }
}
