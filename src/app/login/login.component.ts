import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { LogService } from '../log.service';
import { AuthService } from "angular2-social-login";

import 'rxjs/add/operator/map'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LogService]
})
export class LoginComponent implements OnInit{
  verify: boolean;
  success: boolean;
  verifySuccess: boolean;
  verifyFailed: boolean;

  constructor(private router: Router, public fb: FormBuilder, private logService: LogService, public _auth: AuthService, private route: ActivatedRoute) {
    if (route.snapshot.params['verify']) {
      if (route.snapshot.params['verify'] == "success") {
        this.verifySuccess = true;
      } else if(route.snapshot.params['verify'] == "failed") {
        this.verifyFailed = true;
      }
    }
  }

  public loginForm = this.fb.group({
    email: ["", Validators.required],
    password: ["", Validators.required]
  });

  ngOnInit() {}

  doLogin(event) {
    this.logService.Login(this.loginForm.value.email, this.loginForm.value.password).subscribe(res => {
      localStorage.setItem('token', res.access_token);
      this.logService.getUser().subscribe(user => {
        if (user.status) {
          this.router.navigate(['admin']);
        } else {
          localStorage.removeItem('token');
          this.verify = true;
        }
      })
    });
  }

  SocialLogin(provider) {
    let self = this;
    this._auth.login(provider).subscribe((data) => {
      // console.log(data);
      this.logService.GetSocialUser(data['email']).subscribe(exist => {
        if (exist.count) {
          if (exist.response.provider == provider) {
            this.logService.Login(data['email'], data['uid']).subscribe(res => {
              localStorage.setItem('token', res.access_token);
              this.logService.getUser().subscribe(user => {
                if (user.status) {
                  this.router.navigate(['admin']);
                } else {
                  localStorage.removeItem('token');
                  this.verify = true;
                }
              })
            });
          } else {
            this.logService.ChangeSocialPassword(exist.response.id, data['uid'], provider).subscribe(change => {
              this.logService.Login(data['email'], data['uid']).subscribe(res => {
                localStorage.setItem('token', res.access_token);
                this.logService.getUser().subscribe(user => {
                  if (user.status) {
                    this.router.navigate(['admin']);
                  } else {
                    localStorage.removeItem('token');
                    this.verify = true;
                  }
                })
              })
            })
          }
        } else {
          this.logService.SocialRegister(data['name'], data['email'], data['uid'], provider).subscribe(res => {
            console.log(res);
            // alert('Mail Has Been Sent');
            self.success = true;
            console.log(self.success);
          });
        }
      })
    })
  }
}
