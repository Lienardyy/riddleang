import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';
import { PlayerComponent } from './player/player.component';
import { AdminaddComponent } from './adminadd/adminadd.component';

import { UserService } from './user.service';
import { LogService } from './log.service';
import { RiddleService } from './riddle.service';
import { ScoreService } from './score.service';

import { LoggedInMiddleware, NotLoggedInMiddleware, DeptMiddleware, IsAdminMiddleware, IsPlayerMiddleware, HomeMiddleware } from './middleware';

import { ChangepassComponent } from './changepass/changepass.component';
import { HomeComponent } from './home/home.component';
import { RiddleaddComponent } from './riddleadd/riddleadd.component';
import { RiddleplayComponent } from './riddleplay/riddleplay.component';
import { ScoreComponent } from './score/score.component';
import { SendresetpassComponent } from './sendresetpass/sendresetpass.component';

import { Angular2SocialLoginModule } from "angular2-social-login";
import { RiddledeleteComponent } from './riddledelete/riddledelete.component';
import { RiddleComponent } from './riddle/riddle.component';
import { RiddleeditComponent } from './riddleedit/riddleedit.component';
import { ResetpassComponent } from './resetpass/resetpass.component';

let providers = {
  "google": {
    "clientId": "774344595383-orq1qjkufj0kaml32prenrh54dsjljqd.apps.googleusercontent.com"
  },
  "linkedin": {
    "clientId": "81p20g6jdmztom"
  },
  "facebook": {
    "clientId": "525078371215581",
    "apiVersion": "v2.9" //like v2.4
  }
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    AdminComponent,
    PlayerComponent,
    AdminaddComponent,
    ChangepassComponent,
    HomeComponent,
    RiddleaddComponent,
    RiddleplayComponent,
    ScoreComponent,
    SendresetpassComponent,
    RiddledeleteComponent,
    RiddleComponent,
    RiddleeditComponent,
    ResetpassComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    Angular2SocialLoginModule,
    NgxPaginationModule,
  ],
  providers: [
    UserService,
    LogService,
    LoggedInMiddleware,
    NotLoggedInMiddleware,
    DeptMiddleware,
    IsAdminMiddleware,
    IsPlayerMiddleware,
    HomeMiddleware,
    RiddleService,
    ScoreService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

Angular2SocialLoginModule.loadProvidersScripts(providers);