import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiddledeleteComponent } from './riddledelete.component';

describe('RiddledeleteComponent', () => {
  let component: RiddledeleteComponent;
  let fixture: ComponentFixture<RiddledeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiddledeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiddledeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
