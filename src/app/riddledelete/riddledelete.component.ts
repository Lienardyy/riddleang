import { Component, OnInit } from '@angular/core';
import { RiddleService } from '../riddle.service';
import { Riddle } from '../riddle';

@Component({
  selector: 'app-riddledelete',
  templateUrl: './riddledelete.component.html',
  styleUrls: ['./riddledelete.component.css'],
  providers: [RiddleService]
})
export class RiddledeleteComponent implements OnInit {
  p: number = 1;
  riddles: Riddle[];

  constructor(private riddleService: RiddleService) { }

  ngOnInit() {
      this.index();
  }

  index() {
    this.riddleService.getRiddles().subscribe(riddles => {
      this.riddles = riddles;
    });
  }

  delete(id: number) {
    this.riddleService.delete(id).subscribe((users) => {
      this.index();
      // location.reload();
    });
  }

}
