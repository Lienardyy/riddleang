import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Riddle } from './riddle';

@Injectable()
export class RiddleService {

    constructor(private http: Http) { }

    private riddlesUrl = "http://localhost:8000/api/riddles";

    getRiddles(): Observable<Riddle[]> {
        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.get(this.riddlesUrl, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }

    getRiddle(id: number): Observable<Riddle[]> {
        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.get(this.riddlesUrl + "/" + id, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }

    add(question: string, answer: string): Observable<Riddle[]> {
        var data = JSON.stringify({question: question, answer: answer});
        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",            
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.post(this.riddlesUrl, data, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }

    edit(id: number, question: string, answer: string): Observable<Riddle[]> {
        var data = JSON.stringify({question: question, answer: answer});
        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",            
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });
        console.log(data);
                
        return this.http.put(this.riddlesUrl + "/" + id, data, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }

    delete(id: number): Observable<Riddle[]> {
        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",            
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.delete(this.riddlesUrl + "/" + id, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }
}
