import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

@Injectable()
export class LogService {

  constructor(private router: Router, private http: Http) { }

  Login(email: string, password: string) {
    var headers = new Headers({
        "Content-Type": "application/json",
        "Accept": "application/json"
    });

    let postData = {
        grant_type: "password",
        client_id: 2,
        client_secret: "z20RtUxwaaYXfXOglWmAb9gJMFH72PrLNTUmI3W8",
        username: email,
        password: password,
        scope: ""
    }

  	return this.http.post('http://localhost:8000/oauth/token',  JSON.stringify(postData), {headers: headers})
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  Register(name: string, email: string, password: string) {
    var headers = new Headers({
        "Content-Type": "application/json",
        "Accept": "application/json"
    });

    let postData = {
        name: name,
        email: email,
        password: password,
    }
    console.log(JSON.stringify(postData));
    
  	return this.http.post('http://localhost:8000/api/register2',  JSON.stringify(postData), {headers: headers})
              .map((res: Response) => res.json())
              .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  SocialRegister(name: string, email: string, password: string, provider: string) {
    var headers = new Headers({
        "Content-Type": "application/json",
        "Accept": "application/json"
    });

    let postData = {
        name: name,
        email: email,
        password: password,
        provider: provider,
    }
    console.log(JSON.stringify(postData));
    
  	return this.http.post('http://localhost:8000/api/register2',  JSON.stringify(postData), {headers: headers})
              .map((res: Response) => res.json())
              .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  SendResetPass(email: string) {
    var headers = new Headers({
        "Content-Type": "application/json",
        "Accept": "application/json"
    });

    let postData = {
        email: email,
    }
    // console.log(postData);
    
  	return this.http.post('http://localhost:8000/api/password/email',  JSON.stringify(postData), {headers: headers})
              .map((res: Response) => res.json())
              .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  ResetPass(token: string, email: string, password: string, password_confirmation: string) {
    var headers = new Headers({
        "Content-Type": "application/json",
        // "Accept": "application/json"
    });
    const data = JSON.stringify({
        email: email,
        password: password,
        password_confirmation: password_confirmation,
        token: token,
    });
    console.log(data);
    
  	return this.http.post('http://localhost:8000/api/password/reset',  data, {headers: headers})
              .map((res: Response) => res.json())
              .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getUser() {
    var headers = new Headers({
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + localStorage.getItem('token'),
    });

  	return this.http.get('http://localhost:8000/api/user', {headers: headers})
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  GetSocialUser(email: string) {
    var data = JSON.stringify({email: email});
    var headers = new Headers({
        "Content-Type": "application/json",
        "Accept": "application/json",
    });

  	return this.http.post('http://localhost:8000/api/social', data, {headers: headers})
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  ChangeSocialPassword(id: number, password: string, provider: string) {
      var data = JSON.stringify({password: password, provider: provider});
      // console.log(data);
      var headers = new Headers({
          "Content-Type": "application/json",
          "Accept": "application/json",
      });

      return this.http.put('http://localhost:8000/api/social/' + id, data, {headers: headers})
          .map((res: Response) => {
              return res.json();
          });
  }
  

  IsLoggedIn() {
    var token = localStorage.getItem('token');
    if (token) {
      return true;
    } else {
      return false;
    }
  }

  Logout() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
}
