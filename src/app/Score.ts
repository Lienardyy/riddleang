export class Score {
    constructor(public id: number,
                public user_id: string,
                public score: string,
                public created_at: Date,
                public updated_at: Date,) {
    }
}