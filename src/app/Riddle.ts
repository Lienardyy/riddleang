export class Riddle {
    constructor(public id: number,
                public question: string,
                public answer: string,
                public created_at: Date,
                public updated_at: Date,) {
    }
}