import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AppComponent } from '../app.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { SendresetpassComponent } from '../sendresetpass/sendresetpass.component';
import { ResetpassComponent } from '../resetpass/resetpass.component';

import { AdminComponent } from '../admin/admin.component';
import { PlayerComponent } from '../player/player.component';

import { HomeComponent } from '../home/home.component';

import { AdminaddComponent } from '../adminadd/adminadd.component';
import { UserComponent } from '../user/user.component';
import { RiddleaddComponent } from '../riddleadd/riddleadd.component';
import { RiddleComponent } from '../riddle/riddle.component';
import { RiddleeditComponent } from '../riddleedit/riddleedit.component';
import { RiddledeleteComponent } from '../riddledelete/riddledelete.component';
import { ChangepassComponent } from '../changepass/changepass.component';

import { RiddleplayComponent } from '../riddleplay/riddleplay.component';
import { ScoreComponent } from '../score/score.component';

import { LoggedInMiddleware, NotLoggedInMiddleware, DeptMiddleware, IsAdminMiddleware, IsPlayerMiddleware, HomeMiddleware }  from '../middleware';

const routes: Routes = [
  { path: '',  component: AppComponent, canActivate: [LoggedInMiddleware, DeptMiddleware] },
  { path: 'login',  component: LoginComponent, canActivate: [NotLoggedInMiddleware] },
  { path: 'login/:verify',  component: LoginComponent, canActivate: [NotLoggedInMiddleware] },
  { path: 'register',  component: RegisterComponent, canActivate: [NotLoggedInMiddleware] },
  { path: 'reset',  component: SendresetpassComponent, canActivate: [NotLoggedInMiddleware] },
  { path: 'resetpass/:token',  component: ResetpassComponent, canActivate: [NotLoggedInMiddleware] },
  { path: 'admin',  component: AdminComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware], children:[
    { path: '',  component: HomeComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware, HomeMiddleware] },
    { path: 'home',  component: HomeComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
    { path: 'adminadd',  component: AdminaddComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
    { path: 'user',  component: UserComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
    { path: 'riddleadd',  component: RiddleaddComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
    { path: 'riddle',  component: RiddleComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
    { path: 'riddleedit/:id',  component: RiddleeditComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
    { path: 'riddledelete',  component: RiddledeleteComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
    { path: 'changepass',  component: ChangepassComponent, canActivate: [LoggedInMiddleware, IsAdminMiddleware] },
  ]},
  { path: 'player',  component: PlayerComponent, canActivate: [LoggedInMiddleware, IsPlayerMiddleware], children:[
    { path: '',  component: HomeComponent, canActivate: [LoggedInMiddleware, IsPlayerMiddleware, HomeMiddleware] },
    { path: 'home',  component: HomeComponent, canActivate: [LoggedInMiddleware, IsPlayerMiddleware] },
    { path: 'play',  component: RiddleplayComponent, canActivate: [LoggedInMiddleware, IsPlayerMiddleware] },
    { path: 'score',  component: ScoreComponent, canActivate: [LoggedInMiddleware, IsPlayerMiddleware] },
    { path: 'changepass',  component: ChangepassComponent, canActivate: [LoggedInMiddleware, IsPlayerMiddleware] },
  ]},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
