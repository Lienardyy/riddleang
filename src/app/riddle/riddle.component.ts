import { Component, OnInit } from '@angular/core';
import { RiddleService } from '../riddle.service';
import { Riddle } from '../riddle';

@Component({
  selector: 'app-riddle',
  templateUrl: './riddle.component.html',
  styleUrls: ['./riddle.component.css'],
  providers: [RiddleService]
})
export class RiddleComponent implements OnInit {
  p: number = 1;
  riddles: Riddle[];

  constructor(private riddleService: RiddleService) { }

  ngOnInit() {
      this.index();
  }

  index() {
    this.riddleService.getRiddles().subscribe(riddles => {
      this.riddles = riddles;
    });
  }

}
