import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {User} from './user';


@Injectable()
export class UserService {

    constructor(private http: Http) {}

    private usersUrl = "http://localhost:8000/api/users";

    getUsers(): Observable<User[]> {

        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",            
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.get(this.usersUrl, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }

    addUsers(name: string, email: string): Observable<User[]> {
        var data = JSON.stringify({name: name, email: email});
        // console.log(data);
        var headers = new Headers({
            "Content-Type": "application/json",
            "Accept": "application/json",            
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });
 
        return this.http.post(this.usersUrl, data, {headers: headers})
            .map((res: Response) => {
                // console.log(res);
                return res.json();
            });
    }

    updateUsers(id: number): Observable<User[]> {
        var data = JSON.stringify({id: id});
        // console.log(data);
        var headers = new Headers({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.put(this.usersUrl + "/" + id, data, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }

    changePassword(id: number, password: string): Observable<User[]> {
        var data = JSON.stringify({password: password});
        // console.log(data);
        var headers = new Headers({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + localStorage.getItem('token'),
        });

        return this.http.put(this.usersUrl + "/" + id, data, {headers: headers})
            .map((res: Response) => {
                return res.json();
            });
    }
}
