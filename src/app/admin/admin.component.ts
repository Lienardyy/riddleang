import { Component, OnInit } from '@angular/core';
import { LogService } from '../log.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [LogService]
})
export class AdminComponent implements OnInit {
  changePass: boolean;
  private user;
  constructor(private logService: LogService) { }

  ngOnInit() {
    this.logService.getUser().subscribe(res => {
      if (res.provider == 'none') {
        this.changePass = true;
      }
    })
  }

  logOut() {
    this.logService.Logout();
  }
}
