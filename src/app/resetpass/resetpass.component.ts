import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LogService } from '../log.service';

@Component({
  selector: 'app-resetpass',
  templateUrl: './resetpass.component.html',
  styleUrls: ['./resetpass.component.css'],
  providers: [LogService]
})
export class ResetpassComponent implements OnInit {
  token: string;
  success: boolean;
  failed: boolean;
  diffPass: boolean;

  constructor(public fb: FormBuilder, private logService: LogService, private route: ActivatedRoute) {
    this.token = route.snapshot.params['token'];
  }

  public ResetPassForm = this.fb.group({
    email: ["", Validators.required],
    password: ["", Validators.required],
    confirmPassword: ["", Validators.required],
  });

  ngOnInit() {
  }

  Reset(event) {
    if (this.ResetPassForm.value.confirmPassword != this.ResetPassForm.value.password ) {
      this.diffPass = true;
      this.success = false;
      this.failed = false;
    } else {
      this.logService.ResetPass(this.token, this.ResetPassForm.value.email, this.ResetPassForm.value.password, this.ResetPassForm.value.confirmPassword).subscribe(
        res => {
          this.success = true;
          console.log(res);
        }, error => {
          this.failed = true;
          console.log(error);
        })
    }
  }

}
