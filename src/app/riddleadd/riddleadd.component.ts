import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RiddleService } from '../riddle.service';

@Component({
  selector: 'app-riddleadd',
  templateUrl: './riddleadd.component.html',
  styleUrls: ['./riddleadd.component.css'],
  providers: [RiddleService]
})
export class RiddleaddComponent implements OnInit {
  success: boolean;
  notValid: boolean;

  constructor(public fb: FormBuilder, private riddleService: RiddleService) { }

  public AddRiddleForm = this.fb.group({
    question: ["", Validators.required],
    answer: ["", Validators.required],
  });

  ngOnInit() {
  }

  trim(str){
    return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  }

  add(event) {
    var questionValid = this.AddRiddleForm.value.question.replace(/  +/g, ' ').trim();
    var answerValid = this.AddRiddleForm.value.answer.replace(/  +/g, ' ').trim();
    if (answerValid.indexOf(" ") !== -1) {
      this.notValid = true;
    } else {
      this.notValid = false;
      this.riddleService.add(questionValid, answerValid)
          .subscribe(
                (res) => {
                  console.log(res);
                  this.success = true;
                  this.AddRiddleForm.reset();
                });
    }
  }

}
