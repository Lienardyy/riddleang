import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiddleaddComponent } from './riddleadd.component';

describe('RiddleaddComponent', () => {
  let component: RiddleaddComponent;
  let fixture: ComponentFixture<RiddleaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiddleaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiddleaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
