import { TestBed, inject } from '@angular/core/testing';

import { RiddleService } from './riddle.service';

describe('RiddleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RiddleService]
    });
  });

  it('should be created', inject([RiddleService], (service: RiddleService) => {
    expect(service).toBeTruthy();
  }));
});
