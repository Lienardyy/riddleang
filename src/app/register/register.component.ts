import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Headers, Http, Response } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs/Rx';
import { LogService } from '../log.service';
import { AuthService } from "angular2-social-login";

import 'rxjs/add/operator/map'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [LogService]
})
export class RegisterComponent implements OnInit {
  success: boolean;
  diffPass: boolean;
  // verify: boolean;

  constructor(private router: Router, public fb: FormBuilder, private logService: LogService, public _auth: AuthService) { }

  public registerForm = this.fb.group({
    name: ["", Validators.required],
    email: ["", Validators.required],
    password: ["", Validators.required],
    ConfirmPassword: ["", Validators.required]
  });

  ngOnInit() {
  }

  doRegister(event) {
    if (this.registerForm.value.ConfirmPassword != this.registerForm.value.password ) {
      this.diffPass = true;
      this.success = false;
    } else {
      this.diffPass = false;
      this.logService.Register(this.registerForm.value.name, this.registerForm.value.email, this.registerForm.value.password).subscribe(res => {
        console.log(res);
        this.success = true;
        this.registerForm.reset();
      });
    }
  }

  // SocialRegister(provider) {
  //   this._auth.login(provider).subscribe((data) => {
  //     console.log(data);
  //       this.logService.Register(data['name'], data['email'], data['uid']).subscribe(res => {
  //         this.success = true;
  //         console.log(res);
  //       });
  //     }
  //   )
  // }

  // SocialLogin(provider) {
  //   let self = this;
  //   this._auth.login(provider).subscribe((data) => {
  //     this.logService.GetSocialUser(data['email'], provider).subscribe(exist => {
  //       if (exist.count) {
  //         if (exist.response.provider == provider) {
  //           this.logService.Login(data['email'], data['uid']).subscribe(res => {
  //             localStorage.setItem('token', res.access_token);
  //             this.logService.getUser().subscribe(user => {
  //               if (user.status) {
  //                 this.router.navigate(['admin']);
  //               } else {
  //                 localStorage.removeItem('token');
  //                 this.verify = true;
  //               }
  //             })
  //           });
  //         } else {
  //           this.logService.ChangeSocialPassword(exist.response.id, data['uid'], provider).subscribe(change => {
  //             this.logService.Login(data['email'], data['uid']).subscribe(res => {
  //               localStorage.setItem('token', res.access_token);
  //               this.logService.getUser().subscribe(user => {
  //                 if (user.status) {
  //                   this.router.navigate(['admin']);
  //                 } else {
  //                   localStorage.removeItem('token');
  //                   this.verify = true;
  //                 }
  //               })
  //             })
  //           })
  //         }
  //       } else {
  //         this.logService.SocialRegister(data['name'], data['email'], data['uid'], provider).subscribe(res => {
  //           console.log(res);
  //           self.success = true;
  //           console.log(self.success);
  //         });
  //       }
  //     })
  //   })
  // }
}
