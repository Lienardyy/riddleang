import { Component, OnInit } from '@angular/core';
import { ScoreService } from '../score.service';
import { LogService } from '../log.service';
import { Score } from '../score';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css'],
  providers: [ScoreService, LogService]
})
export class ScoreComponent implements OnInit {
  p: number = 1;
  scores: Score[];

  constructor(private scoreService: ScoreService, private logService: LogService) { }

  ngOnInit() {
      this.index();
  }

  index() {
    this.logService.getUser().subscribe(user => {
      this.scoreService.getScores(user.id).subscribe(score => {
        this.scores = score;
      })
    })
  }

}
