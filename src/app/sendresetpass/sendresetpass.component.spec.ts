import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendresetpassComponent } from './sendresetpass.component';

describe('SendresetpassComponent', () => {
  let component: SendresetpassComponent;
  let fixture: ComponentFixture<SendresetpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendresetpassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendresetpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
