import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LogService } from '../log.service';

@Component({
  selector: 'app-sendresetpass',
  templateUrl: './sendresetpass.component.html',
  styleUrls: ['./sendresetpass.component.css'],
  providers: [LogService]
})
export class SendresetpassComponent implements OnInit {
  success: boolean;
  social: boolean;
  noExist: boolean;

  constructor(public fb: FormBuilder, private logService: LogService) { }

  public SendResetPassForm = this.fb.group({
    email: ["", Validators.required],
  });

  ngOnInit() {
  }

  Send(event) {
    if (this.SendResetPassForm.value.email == '') {
      return false;
    }
    this.logService.GetSocialUser(this.SendResetPassForm.value.email).subscribe(exist => {
      console.log(exist);
      if (exist.count) {
        if (exist.response.provider == 'none') {
          this.logService.SendResetPass(this.SendResetPassForm.value.email).subscribe(res => {
            this.success = true;
            console.log(res);
          })
        } else {
          this.social = true;
        }
      } else {
        this.noExist = true;
      }
    })
  }

}
