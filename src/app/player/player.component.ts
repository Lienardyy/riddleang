import { Component, OnInit } from '@angular/core';
import { LogService } from '../log.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
  providers: [LogService]
})
export class PlayerComponent implements OnInit {
  changePass: boolean;

  constructor(private logService: LogService) { }

  ngOnInit() {
    this.logService.getUser().subscribe(res => {
      if (res.provider == 'none') {
        this.changePass = true;
      }
    })
  }

  logOut() {
    this.logService.Logout();
  }
}
