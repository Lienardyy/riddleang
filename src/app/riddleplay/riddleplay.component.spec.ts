import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiddleplayComponent } from './riddleplay.component';

describe('RiddleplayComponent', () => {
  let component: RiddleplayComponent;
  let fixture: ComponentFixture<RiddleplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiddleplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiddleplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
