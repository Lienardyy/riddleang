import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RiddleService } from '../riddle.service';
import { ScoreService } from '../score.service';
import { LogService } from '../log.service';
import { Riddle } from '../riddle';

@Component({
  selector: 'app-riddleplay',
  templateUrl: './riddleplay.component.html',
  styleUrls: ['./riddleplay.component.css'],
  providers: [RiddleService, ScoreService, LogService]
})
export class RiddleplayComponent implements OnInit {
  riddles: Riddle[];
  emptyBox : boolean;

  constructor(private router: Router, public fb: FormBuilder, private riddleService: RiddleService, private scoreService: ScoreService, private logService: LogService) { }

  public PlayRiddleForm = this.fb.group({
    input0: ["", Validators.required],
    input1: ["", Validators.required],
    input2: ["", Validators.required],
    input3: ["", Validators.required],
    input4: ["", Validators.required],
    input5: ["", Validators.required],
    input6: ["", Validators.required],
    input7: ["", Validators.required],
    input8: ["", Validators.required],
    input9: ["", Validators.required],
  });

  ngOnInit() {
      this.index();
  }

  index() {
      this.riddleService.getRiddles()
          .subscribe(
              riddles => {
                  this.riddles = riddles;
              });
  }

  getDimensionsByIndex(i){
    return this.riddles['response'][i];
  }

  result(event) {
    var result = 0;
    var empty = 0;
    for (var index = 0; index < 10; index++) {
      let input = this.PlayRiddleForm.controls["input"+index].value;
      let answer = this.getDimensionsByIndex(index).answer;
      // console.log(input + "=" + answer);
      if (input == "") {
        empty++;
      }
      if (input.toLowerCase() == answer.toLowerCase()) {
        result++;
      }
    }
    if (empty > 0) {
      this.emptyBox = true;
      return false;
    }
    this.logService.getUser().subscribe(user => {
      this.scoreService.addScore(user.id, result).subscribe(score => {
        this.router.navigate(['player/score']);
      })
    })
    // console.log(this.score);
  }

}
