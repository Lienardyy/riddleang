import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'app-adminadd',
  templateUrl: './adminadd.component.html',
  styleUrls: ['./adminadd.component.css'],
  providers: [UserService]
})
export class AdminaddComponent implements OnInit {

  constructor(public fb: FormBuilder, private userService: UserService) { }

  public AddAdminForm = this.fb.group({
    name: ["", Validators.required],
    email: ["", Validators.required],
  });

  ngOnInit() {
  }

  add(event) {
    this.userService.addUsers(this.AddAdminForm.value.name, this.AddAdminForm.value.email)
        .subscribe(
              (users) => {
                //this.users = users;
                console.log(users);
                //location.reload();
              });
  }
}
