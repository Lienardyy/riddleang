import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [UserService]
})
export class UserComponent implements OnInit {
  p: number = 1;
  users: User[];

  constructor(private userService: UserService) {}

  ngOnInit() {
      this.index();
  }

  index() {
    this.userService.getUsers().subscribe(users => {
        this.users = users;
    });
  }

  update(id: number) {
    this.userService.updateUsers(id).subscribe((users) => {
        this.index();
        // location.reload();
    });
  }

}
