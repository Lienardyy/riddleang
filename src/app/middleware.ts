import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { LogService } from './log.service';

@Injectable()
export class LoggedInMiddleware implements CanActivate {
  constructor(private router: Router, private logService: LogService) {}

  canActivate() {
    if (this.logService.IsLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}
@Injectable()
export class NotLoggedInMiddleware implements CanActivate {
  constructor(private router: Router, private logService: LogService) {}

  canActivate() {
    if (this.logService.IsLoggedIn()) {
      this.router.navigate(['']);
      return false;
    } else {
      return true;
    }
  }
}
@Injectable()
export class DeptMiddleware implements CanActivate {
  constructor(private router: Router, private logService: LogService) {}

  canActivate() {
    return this.logService.getUser().map((res) => {
      if (res.department == "ADMIN") {
        this.router.navigate(['admin/home']);
        return true;
      } else {
        this.router.navigate(['player/home']);
        return false;
      }
    })
  }
}
@Injectable()
export class IsAdminMiddleware implements CanActivate {
  constructor(private router: Router, private logService: LogService) {}


  canActivate() {
    return this.logService.getUser().map((res) => {
      if (res.department == "ADMIN") {
        return true;
      } else {
        this.router.navigate(['']);
        return false;
      }
    })
  }
}
@Injectable()
export class IsPlayerMiddleware implements CanActivate {
  constructor(private router: Router, private logService: LogService) {}

  canActivate() {
    return this.logService.getUser().map((res) => {
      if (res.department == "PLAYER") {
        return true;
      } else {
        this.router.navigate(['']);
        return false;
      }
    })
  }
}
@Injectable()
export class HomeMiddleware implements CanActivate {
  constructor(private router: Router) {}

  canActivate() {
    this.router.navigate(['']);
    return false;
  }
}